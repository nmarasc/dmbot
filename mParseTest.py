# MapParse test file

from Model.Map import mapParser
from Model.Map import Map
from Model.Actor import parseActorFromFile

d = mapParser("Templates/demomap")
d2 = parseActorFromFile("Templates/demoActor.json")
d3 = d2[1]
print(d)

m = Map(d, d3)
for x in range(m.mapX):
        for y in range(m.mapY):
            print(m.dmap[x][y], end='')
        print()
