# Python imports
import os
# Controller imports
from Controller import CmdParser
# Model imports
from Model.Map import Map, mapParser
from Model.Actor import parseActorFromFile as actParser

MAP_PATH = 'Templates/demomap'
ACT_PATH = 'Templates/actors'

map_inst = None

def beginLoop():
    global map_inst
    map_Dict, act_List = parseData(MAP_PATH, ACT_PATH)
    map_inst = Map(map_Dict, act_List)
    running = True
    print("Welcome to DMBot!")
    while running:
        user_input = input("Enter your command> ")
        cmd_key, cmd_args = CmdParser.parseCommand(user_input.strip(), map_inst)
        if cmd_key is None: # Error occurred
            print(cmd_args)
        else:
            running = handleCommand(cmd_key, cmd_args)

def handleCommand(cmd_key, cmd_args):
    # We're doing this dumb and gross because time is of the essence
    if cmd_key == 'MAPI':
        print(map_inst)
    elif cmd_key == 'SPCI':
        print(cmd_args)
    elif cmd_key == 'ACTI':
        print(cmd_args)
    elif cmd_key == 'HELP':
        print(
          'The following commands are available: \n',
          'HELP - list of help commands\n',
          'QUIT - exit the program\n',
          'ACTI [actor name] - display information ',
            'about an actor with a given name')
    elif cmd_key == 'QUIT':
        print('Goodbye scrub')
        return False
    else: # The dreaded MOVE command
       result, msg = map_inst.moveActor(cmd_args[0], cmd_args[1], cmd_args[2])
       print(msg)

    return True

def parseData(mpath, apath):
    map_dict = mapParser(mpath)
    try: # Actor path is a directory
        root,_,files = next(os.walk(apath))
        act_dict = [actParser(root+"/"+f) for f in files]
    except StopIteration: # Actor path is a file or at least not a dir
        act_dict = [actParser(apath)]
    return map_dict, act_dict
