# Python imports
import random

# Feature constants
FEATURES = {
    'FOREST' : {
        'ROCKS' : {
            'LARGE' : 10,
            'MEDIUM' : 5,
            'SMALL' : 5
        },
        'STICKS' : {
            'LARGE' : 10,
            'MEDIUM' : 5,
            'SMALL' : 5
        },
        'MOSS' : {
            'THICK' : 10,
            'SPARSE' : 5
        }
    },
    'DUNGEON' : {
        'BONES' : {
            'MANY' : 10,
            'FEW' : 5
        },
        'STONE' : {
            'COBBLE' : 5,
            'CRACKED' : 5
        }
    }
}

HAZARDS = {
    'FOREST' : {
    'BEARTRAP': 'IMMOBILE',
    'HOLE' : 'DEAD',
    'ROOT' : 'STUNNED',
    'PUNGIS' : 'SUPERDEAD'
    },
    'DUNGEON' : {
    'RAZORWIRE': 'IMMOBILE',
    'HOLE' : 'DEAD',
    'BONE TRAP' : 'DEAD'
    }
}

class Space:

    # Space constructor
    def __init__(self, terrain, cost = None):

        self.occupied = False
        self.hazards = []
        self.terrain = terrain
        self.feature = _generateFeature(self.terrain)

        if cost:
            self.cost = cost
        else:
            flist = self.feature.split()
            self.cost = FEATURES[self.terrain][flist[1]][flist[0]]

    # Get occupied status
    def getOccupied(self):
        return self.occupied

    # Set occupied status
    def setOccupied(self, occupied):
        self.occupied = occupied

    # Get terrain value
    def getTerrain(self):
        return self.terrain

    # Set terrain value
    def setTerrain(self, terrain):
        pass

    # Get hazards value
    def getHazards(self):
        return self.hazards

    # Set hazards value
    def setHazards(self, hazards):
        self.hazards = hazards

    # Get feature value
    def getFeature(self):
        return self.feature

    # Set feature value
    def setFeature(self, feature):
        pass

    # Get cost value
    def getCost(self):
        return self.cost

    # Set cost value
    def setCost(self, cost):
        pass

    # Space to string
    def __str__(self):
        return f'Occupied : {self.occupied}\n' \
               f'Terrain : {self.terrain}\n' \
               f'Hazards : {self.hazards}\n' \
               f'Feature : {self.feature}\n' \
               f'Cost : {self.cost}'

# Randomly pick a terrain feature
def _generateFeature(terrain):
    features = FEATURES[terrain]
    main_feature = random.choice(list(features.keys()))
    features = features[main_feature]
    sub_feature = random.choice(list(features.keys()))
    return f'{sub_feature} {main_feature}'

# Generate list containing requested number of spaces
def getSpace(terrain, cost = None, num_spaces = 1):
    return list(Space(terrain, cost) for index in range(num_spaces))

#generate a random hazard
def _generateRandomHazard(count = 1, s = None):
    my_new_hazards = []
    potential_hazards = HAZARDS[s.getTerrain()]
    for index in range(count):
        my_new_hazards.append(random.choice(list(potential_hazards)))
    return my_new_hazards
